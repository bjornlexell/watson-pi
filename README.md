####Node-red:
<pi-ip>:1880

####Site entrypoint (served by Node-red)
<pi-ip>:1880/classify

####Change API key
Open <pi-ip>:1880, go to "sheet 1" and change the query string "api_key" in the "Call Watson-node"

####Set up
Put flows_raspberrypi.json in:
/home/pi/.node-red

Put classify.html and results.html in:
/home/pi/

Image will appear in:
/home/pi/images (might need to create this folder manually)

####Boot with fullscreen epihany web browser (keyboard is needed to exit the browser)

Add the line @/home/pi/Desktop/fullscreen.sh in:
/home/pi/.config/lxsession/LXDE-pi/autostart

Put fullscreen.sh in:
/home/pi/Desktop/fullscreen.sh

####Firefox as browser (possible to minimize with touchscreen):

Start firefox from GUI(Start -> Internet), press settings button and choose fullscreen.

####On-screen keyboard
Run keyboard.sh from any folder